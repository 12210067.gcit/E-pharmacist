import { showAlert } from "./alert.js"

document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault()
    console.log("hiii")
    const name = document.getElementById('name').value
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('password-confirm').value
    const role = document.querySelector('input[name=role]:checked').value
    signup(name, email, password, passwordConfirm, role)

})

const signup = async (name, email, password, passwordConfirm, role) => {
    try {
        console.log("hii22")
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:5005/api/v1/users/signup',
            data: {
                name,
                email,
                password,
                passwordConfirm,
                role,
            },
        })
        console.log("hii25")
        if (res.data.status === 'success') {
            console.log("hii255")
            showAlert('success', 'Account created successfully!')
            window.setTimeout(() => {
                location.assign('/')
            }, 1500)
            var obj = res.data.data.user
            document.cookie = ' token = ' + JSON.stringify(obj)
        }
    } catch (err) {
        console.log(err)
        let message =
            typeof err.response !== 'undefined' ? err.response.data.message : err.message
        showAlert('error', 'Error: passwords are not the same', message)
    }
}

